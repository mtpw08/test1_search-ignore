# config\routes.rb
MyBookshop2::Application.routes.draw do

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"


  # post from _search.html.erb 'partial' form,
  # map to the products controller search action

  post 'products/search', to: 'products#search'

  resources :products

 end
